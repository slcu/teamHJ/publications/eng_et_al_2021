import os
import argparse
import vtk
from shutil import copy 

def print_members(object):
    for m in [method_name for method_name in dir(object)
              if callable(getattr(object, method_name))]:
        print m


def write_vertical_walls(file_name):
    # Read the source file.
    if file_name.endswith(".vtk"):
        reader = vtk.vtkPolyDataReader()
    elif file_name.endswith(".ply"):
        reader = vtk.vtkPLYReader()
    else:
        print("file type not recognised")
        return

    fname = os.path.splitext(os.path.basename(file_name))[0]
    dirname = os.path.dirname(file_name)

    reader.SetFileName(file_name)
    reader.Update()  # Needed because of GetScalarRange
    pcPolyData = reader.GetOutput()
    # scalar_range = pcPolyData.GetScalarRange()

    print("polydata num points: {}".format(pcPolyData.GetNumberOfPoints()))
    print("Getting vertical points from: {}".format(file_name))
    numPoints = pcPolyData.GetNumberOfPoints()

    normals = pcPolyData.GetPointData().GetNormals()

    vertical_points = []

    threshold_z = 0.5

    for i in xrange(numPoints):
        if abs(normals.GetTuple3(i)[2]) <= threshold_z:
            vertical_points.append(i)
            # print(normals.GetTuple3(i))

    # savepath = os.path.join(dirname, '{}_vertical_points_{}.txt'.format(fname, len(vertical_points)))

    model_file = "./iso_inflate.model"
    
    copy(model_file, os.path.dirname(file_name))

    with open(os.path.join(os.path.dirname(file_name), model_file), 'a') as f:
        f.write("\n\n")
        f.write("VertexNoUpdateFromIndex 0 1 {}\n".format(len(vertical_points)))

        counter = int(0)
        for item in vertical_points:
            f.write("%s " % str(item))
            counter += 1
        if counter % 10 == 0:
            f.write("\n")

    print("numpoints: {}".format(numPoints))
    print("num vertical points: {}".format(len(vertical_points)))


# if __name__ == '__main__':
#     parser = argparse.ArgumentParser()
#     parser.add_argument("file_name")
#     args = parser.parse_args()
#     write_vertical_walls(args.file_name)
