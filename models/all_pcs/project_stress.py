import os
import vtk
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import tifffile as tf
from tqdm import tqdm
import argparse


def print_members(object):
    for m in [method_name for method_name in dir(object)
              if callable(getattr(object, method_name))]:
        print(m)


def read_vtk(vtk_path):
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtk_path)
    reader.Update()
    pcPolyData = reader.GetOutput()
    return pcPolyData


def project_stress(stress_mat, strain_mat, stress_aniso_mat, vtk_path, mt_path=None):
    if os.path.exists(vtk_path):
        pcPolyData = read_vtk(vtk_path)
        numCells = pcPolyData.GetNumberOfCells()
        stress = pcPolyData.GetCellData().GetArray("cell variable 7")
        strain = pcPolyData.GetCellData().GetArray("cell variable 11")
        stress_aniso = pcPolyData.GetCellData().GetArray("cell variable 18")

        for i in range(numCells):
            stress_val = stress.GetValue(i)
            strain_val = strain.GetValue(i)
            stress_aniso_val = stress_aniso.GetValue(i)
            # print(stress_val, strain_val)
            cell = pcPolyData.GetCell(i)
            points = cell.GetPoints()
            np_pts = np.array([points.GetPoint(i)
                               for i in range(points.GetNumberOfPoints())])
            centroid = np_pts.mean(axis=0)
            if centroid[2] > 25.0:
                stress_mat[int(centroid[1]), int(centroid[0])] = stress_val
                strain_mat[int(centroid[1]), int(centroid[0])] = strain_val
                stress_aniso_mat[int(centroid[1]), int(
                    centroid[0])] = stress_aniso_val
    return stress_mat, strain_mat, stress_aniso_mat


def main(d):
    # d = "./"
    d = os.path.abspath(d)
    dirs = [os.path.join(d, o) for o in os.listdir(d)
            if os.path.isdir(os.path.join(d, o)) and "pc" in o]

    vtu_paths = [dire + "/vtk/VTK_cells000050.vtu" for dire in dirs]

    stress_mat = np.zeros([887, 840])
    strain_mat = np.zeros([887, 840])
    strain_aniso_mat = np.zeros([887, 840])

    for vtu_file in tqdm(vtu_paths):
        stress_mat, strain_mat, strain_aniso_mat = project_stress(
            stress_mat, strain_mat, strain_aniso_mat, vtu_file)

    gf_stress = gaussian_filter(stress_mat.T * 1000, sigma=2)
    gf_strain = gaussian_filter(strain_mat.T * 1000, sigma=2)
    gf_aniso = gaussian_filter(strain_aniso_mat.T * 1000, sigma=2)

    tf.imsave('./stress_proj.tif', gf_stress.astype(np.uint16), imagej=True)
    tf.imsave('./strain_proj.tif', gf_strain.astype(np.uint16), imagej=True)
    tf.imsave('./stress_aniso_proj.tif',
              gf_aniso.astype(np.uint16), imagej=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir")
    args = parser.parse_args()
    main(args.data_dir)
