import os
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from PIL import Image
from mesh_from_2d_seg import tvtk_output_2d, rgb2id_array
from verticalWalls import write_vertical_walls
from shutil import copy

def path2id_array(image_path):
    """return identifier array from image path"""
    seg_image = Image.open(image_path)
    seg_image_array_rgb = np.asarray(seg_image, dtype="uint64")
    cid_array = rgb2id_array(seg_image_array_rgb)
    return cid_array


def make_directory(cell_id):
    dir_name = "./pc_{}".format(cell_id)
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    return dir_name


def main():

    im_path = "2D_PM_LTIi6b-GFP mChTUA5 WT 48h S1.tifws_seg.png"
    seg_image = path2id_array(im_path)

    # plt.imshow(seg_image)
    # plt.show()

    cids = np.unique(seg_image)[1:]

    for cid in tqdm(cids):
        print(cid)
        dir_name = make_directory(cid)
        ply_fname = tvtk_output_2d(im_path, dir_name, False, cid)
        write_vertical_walls(ply_fname)
        copy("./solver.rk5", dir_name)

if __name__ == '__main__':
    main()